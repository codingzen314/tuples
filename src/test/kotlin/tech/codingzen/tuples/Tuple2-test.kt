package tech.codingzen.tuples

import org.junit.Test
import kotlin.test.assertEquals

class Tuple2Test {
  @Test
  fun create() {
    val p1 = "a"
    val p2 = 2
    val tuple = Tuple2(p1, p2)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
  }

  @Test
  fun shorthand_accessor() {
    val p1 = "a"
    val p2 = 2
    val tuple = Tuple2(p1, p2)
    assertEquals(p1, tuple._1)
    assertEquals(p2, tuple._2)
  }

  @Test
  fun map1() {
    val p1 = "foo"
    val p2 = 2
    val q1 = 3
    val original = Tuple2(p1, p2)
    val updated = original.map1(String::length)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(q1, updated._1)
    assertEquals(p2, updated._2)
  }

  @Test
  fun map2() {
    val p1 = "foo"
    val p2 = 2
    val q2 = 3
    val original = Tuple2(p1, p2)
    val updated = original.map2 { it + 1 }
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
  }

  @Test
  fun append1() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'c'
    val original = Tuple2(p1, p2)
    val updated = original.append(p3)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
  }

  @Test
  fun append2() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'c'
    val p4 = "far"
    val original = Tuple2(p1, p2)
    val updated = original.append(p3, p4)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
  }

  @Test
  fun append3() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'c'
    val p4 = "far"
    val p5 = 6
    val original = Tuple2(p1, p2)
    val updated = original.append(p3, p4, p5)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun append4() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'c'
    val p4 = "far"
    val p5 = 6
    val p6 = 6
    val original = Tuple2(p1, p2)
    val updated = original.append(p3, p4, p5, p6)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun insert1() {
    val p1 = 5
    val p2 = 'a'
    val q1 = "f"
    val original = Tuple2(p1, p2)
    val updated = original.insert1(q1)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(q1, updated._1)
    assertEquals(p1, updated._2)
    assertEquals(p2, updated._3)
  }

  @Test
  fun insert2() {
    val p1 = 5
    val p2 = 'a'
    val q2 = "f"
    val original = Tuple2(p1, p2)
    val updated = original.insert2(q2)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
    assertEquals(p2, updated._3)
  }

  @Test
  fun set1() {
    val p1 = 5
    val p2 = 'a'
    val q1 = "f"
    val original = Tuple2(p1, p2)
    val updated = original.set1(q1)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(q1, updated._1)
    assertEquals(p2, updated._2)
  }

  @Test
  fun set2() {
    val p1 = 5
    val p2 = 'a'
    val q2 = "f"
    val original = Tuple2(p1, p2)
    val updated = original.set2(q2)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
  }
}