package tech.codingzen.tuples

import org.junit.Test
import kotlin.test.assertEquals

class TupleTest {
  @Test
  fun tuple1_create() {
    val p1 = "a"
    val tuple = Tuple(p1)
    assertEquals(p1, tuple.part1)
  }

  @Test
  fun tuple2_create() {
    val p1 = "a"
    val p2 = 3
    val tuple = Tuple(p1, p2)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
  }

  @Test
  fun tuple3_create() {
    val p1 = "a"
    val p2 = 3
    val p3 = 'c'
    val tuple = Tuple(p1, p2, p3)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
    assertEquals(p3, tuple.part3)
  }

  @Test
  fun tuple4_create() {
    val p1 = "a"
    val p2 = 3
    val p3 = 'c'
    val p4 = "b"
    val tuple = Tuple(p1, p2, p3, p4)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
    assertEquals(p3, tuple.part3)
    assertEquals(p4, tuple.part4)
  }

  @Test
  fun tuple5_create() {
    val p1 = "a"
    val p2 = 3
    val p3 = 'c'
    val p4 = "b"
    val p5 = 5
    val tuple = Tuple(p1, p2, p3, p4, p5)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
    assertEquals(p3, tuple.part3)
    assertEquals(p4, tuple.part4)
    assertEquals(p5, tuple.part5)
  }

  @Test
  fun tuple6_create() {
    val p1 = "a"
    val p2 = 3
    val p3 = 'c'
    val p4 = "b"
    val p5 = 5
    val p6 = "e"
    val tuple = Tuple(p1, p2, p3, p4, p5, p6)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
    assertEquals(p3, tuple.part3)
    assertEquals(p4, tuple.part4)
    assertEquals(p5, tuple.part5)
    assertEquals(p6, tuple.part6)
  }

}