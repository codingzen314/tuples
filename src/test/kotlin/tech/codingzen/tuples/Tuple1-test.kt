package tech.codingzen.tuples

import org.junit.Test
import kotlin.test.assertEquals

class Tuple1Test {
  @Test
  fun create() {
    val p1 = "a"
    val tuple = Tuple1(p1)
    assertEquals(p1, tuple.part1)
  }

  @Test
  fun shorthand_accessor() {
    val p1 = "a"
    val tuple = Tuple1(p1)
    assertEquals(p1, tuple._1)
  }

  @Test
  fun map1() {
    val p1 = "foo"
    val q1 = 3
    val original = Tuple1(p1)
    val updated = original.map1(String::length)
    assertEquals(p1, original._1)
    assertEquals(q1, updated._1)
  }

  @Test
  fun append1() {
    val p1 = 5
    val p2 = "e"
    val original = Tuple1(p1)
    val updated = original.append(p2)
    assertEquals(p1, original._1)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
  }

  @Test
  fun append2() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'd'
    val original = Tuple1(p1)
    val updated = original.append(p2, p3)
    assertEquals(p1, original._1)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
  }

  @Test
  fun append3() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'd'
    val p4 = 12
    val original = Tuple1(p1)
    val updated = original.append(p2, p3, p4)
    assertEquals(p1, original._1)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
  }

  @Test
  fun append4() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'd'
    val p4 = 12
    val p5 = "foo"
    val original = Tuple1(p1)
    val updated = original.append(p2, p3, p4, p5)
    assertEquals(p1, original._1)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun append5() {
    val p1 = 5
    val p2 = "e"
    val p3 = 'd'
    val p4 = 12
    val p5 = "foo"
    val p6 = 13
    val original = Tuple1(p1)
    val updated = original.append(p2, p3, p4, p5, p6)
    assertEquals(p1, original._1)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun insert1() {
    val p1 = 5
    val q1 = "f"
    val original = Tuple1(p1)
    val updated = original.insert1(q1)
    assertEquals(p1, original._1)
    assertEquals(q1, updated._1)
    assertEquals(p1, updated._2)
  }

  @Test
  fun set1() {
    val p1 = 5
    val q1 = "f"
    val original = Tuple1(p1)
    val updated = original.set1(q1)
    assertEquals(p1, original._1)
    assertEquals(q1, updated._1)
  }

}