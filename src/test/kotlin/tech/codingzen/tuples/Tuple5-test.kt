package tech.codingzen.tuples

import org.junit.Test
import kotlin.test.assertEquals

class Tuple5Test {
  @Test
  fun create() {
    val p1 = "a"
    val p2 = 2
    val p3 = 'b'
    val p4 = "foo"
    val p5 = 5
    val tuple = Tuple5(p1, p2, p3, p4, p5)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
    assertEquals(p3, tuple.part3)
    assertEquals(p4, tuple.part4)
    assertEquals(p5, tuple.part5)
  }

  @Test
  fun shorthand_accessor() {
    val p1 = "a"
    val p2 = 2
    val p3 = 'b'
    val p4 = "foo"
    val p5 = 5
    val tuple = Tuple5(p1, p2, p3, p4, p5)
    assertEquals(p1, tuple._1)
    assertEquals(p2, tuple._2)
    assertEquals(p3, tuple._3)
    assertEquals(p4, tuple._4)
    assertEquals(p5, tuple._5)
  }

  @Test
  fun map1() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val q1 = 3
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.map1(String::length)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(q1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun map2() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val q2 = 3
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.map2 { it + 1 }
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun map3() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val q3 = 'P'
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.map3(Character::toUpperCase)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(q3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun map4() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val q4 = 3
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.map4(String::length)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(q4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun map5() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val q5 = 6
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.map5 { it + 1 }
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(q5, updated._5)
  }

  @Test
  fun append1() {
    val p1 = 5
    val p2 = "e"
    val p3 = "f"
    val p4 = 'c'
    val p5 = "foo"
    val p6 = 5
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.append(p6)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun insert1() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 34
    val p5 = "e"
    val q1 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.insert1(q1)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(q1, updated._1)
    assertEquals(p1, updated._2)
    assertEquals(p2, updated._3)
    assertEquals(p3, updated._4)
    assertEquals(p4, updated._5)
    assertEquals(p5, updated._6)
  }

  @Test
  fun insert2() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 34
    val p5 = "e"
    val q2 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.insert2(q2)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
    assertEquals(p2, updated._3)
    assertEquals(p3, updated._4)
    assertEquals(p4, updated._5)
    assertEquals(p5, updated._6)
  }

  @Test
  fun insert3() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 34
    val p5 = "e"
    val q3 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.insert3(q3)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(q3, updated._3)
    assertEquals(p3, updated._4)
    assertEquals(p4, updated._5)
    assertEquals(p5, updated._6)
  }

  @Test
  fun insert4() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 34
    val p5 = "e"
    val q4 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.insert4(q4)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(q4, updated._4)
    assertEquals(p4, updated._5)
    assertEquals(p5, updated._6)
  }

  @Test
  fun insert5() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 34
    val p5 = "e"
    val q5 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.insert5(q5)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(q5, updated._5)
    assertEquals(p5, updated._6)
  }

  @Test
  fun set1() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val q1 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.set1(q1)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(q1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun set2() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val q2 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.set2(q2)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun set3() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val q3 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.set3(q3)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(q3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun set4() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val q4 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.set4(q4)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(q4, updated._4)
    assertEquals(p5, updated._5)
  }

  @Test
  fun set5() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val q5 = "f"
    val original = Tuple5(p1, p2, p3, p4, p5)
    val updated = original.set5(q5)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(q5, updated._5)
  }
}