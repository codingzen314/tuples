package tech.codingzen.tuples

import org.junit.Test
import kotlin.test.assertEquals

class Tuple6Test {
  @Test
  fun create() {
    val p1 = "a"
    val p2 = 2
    val p3 = 'b'
    val p4 = "foo"
    val p5 = 5
    val p6 = 6
    val tuple = Tuple6(p1, p2, p3, p4, p5, p6)
    assertEquals(p1, tuple.part1)
    assertEquals(p2, tuple.part2)
    assertEquals(p3, tuple.part3)
    assertEquals(p4, tuple.part4)
    assertEquals(p5, tuple.part5)
    assertEquals(p6, tuple.part6)
  }

  @Test
  fun shorthand_accessor() {
    val p1 = "a"
    val p2 = 2
    val p3 = 'b'
    val p4 = "foo"
    val p5 = 5
    val p6 = 'e'
    val tuple = Tuple6(p1, p2, p3, p4, p5, p6)
    assertEquals(p1, tuple._1)
    assertEquals(p2, tuple._2)
    assertEquals(p3, tuple._3)
    assertEquals(p4, tuple._4)
    assertEquals(p5, tuple._5)
    assertEquals(p6, tuple._6)
  }

  @Test
  fun map1() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val p6 = 5
    val q1 = 3
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.map1(String::length)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(q1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun map2() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val p6 = 5
    val q2 = 3
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.map2 { it + 1 }
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun map3() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val p6 = 'r'
    val q3 = 'P'
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.map3(Character::toUpperCase)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(q3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun map4() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val p6 = 1
    val q4 = 3
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.map4(String::length)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(q4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun map5() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val p6 = 4
    val q5 = 6
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.map5 { it + 1 }
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(q5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun map6() {
    val p1 = "foo"
    val p2 = 2
    val p3 = 'p'
    val p4 = "foo"
    val p5 = 5
    val p6 = 4
    val q6 = 5
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.map6 { it + 1 }
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(q6, updated._6)
  }

  @Test
  fun set1() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val p6 = "er"
    val q1 = "f"
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.set1(q1)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(q1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun set2() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val q2 = "f"
    val p6 = 'a'
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.set2(q2)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(q2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun set3() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val p6 = "abc"
    val q3 = "f"
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.set3(q3)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(q3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun set4() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val p6 = 3
    val q4 = "f"
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.set4(q4)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(q4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun set5() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val p6 = "abc"
    val q5 = "f"
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.set5(q5)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(q5, updated._5)
    assertEquals(p6, updated._6)
  }

  @Test
  fun set6() {
    val p1 = 5
    val p2 = 'a'
    val p3 = "foo"
    val p4 = 7
    val p5 = 'g'
    val p6 = "abc"
    val q6 = "f"
    val original = Tuple6(p1, p2, p3, p4, p5, p6)
    val updated = original.set6(q6)
    assertEquals(p1, original._1)
    assertEquals(p2, original._2)
    assertEquals(p3, original._3)
    assertEquals(p4, original._4)
    assertEquals(p5, original._5)
    assertEquals(p6, original._6)
    assertEquals(p1, updated._1)
    assertEquals(p2, updated._2)
    assertEquals(p3, updated._3)
    assertEquals(p4, updated._4)
    assertEquals(p5, updated._5)
    assertEquals(q6, updated._6)
  }
}