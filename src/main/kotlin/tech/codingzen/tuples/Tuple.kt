package tech.codingzen.tuples

object Tuple {
    operator fun <P1> invoke(part1: P1): Tuple1<P1> = Tuple1(part1)
    operator fun <P1, P2> invoke(part1: P1, part2: P2): Tuple2<P1, P2> = Tuple2(part1, part2)
    operator fun <P1, P2, P3> invoke(part1: P1, part2: P2, part3: P3): Tuple3<P1, P2, P3> = Tuple3(part1, part2, part3)
    operator fun <P1, P2, P3, P4> invoke(part1: P1, part2: P2, part3: P3, part4: P4): Tuple4<P1, P2, P3, P4> = Tuple4(part1, part2, part3, part4)
    operator fun <P1, P2, P3, P4, P5> invoke(part1: P1, part2: P2, part3: P3, part4: P4, part5: P5): Tuple5<P1, P2, P3, P4, P5> = Tuple5(part1, part2, part3, part4, part5)
    operator fun <P1, P2, P3, P4, P5, P6> invoke(part1: P1, part2: P2, part3: P3, part4: P4, part5: P5, part6: P6): Tuple6<P1, P2, P3, P4, P5, P6> = Tuple6(part1, part2, part3, part4, part5, part6)
}