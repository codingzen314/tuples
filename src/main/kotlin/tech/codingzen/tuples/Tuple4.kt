package tech.codingzen.tuples

data class Tuple4<out P1, out P2, out P3, out P4>(val part1: P1, val part2: P2, val part3: P3, val part4: P4)
fun <P1, P2, P3, P4, Q1> Tuple4<P1, P2, P3, P4>.map1(f: (P1) -> Q1): Tuple4<Q1, P2, P3, P4> = Tuple4(f(part1), part2, part3, part4)
fun <P1, P2, P3, P4, Q2> Tuple4<P1, P2, P3, P4>.map2(f: (P2) -> Q2): Tuple4<P1, Q2, P3, P4> = Tuple4(part1, f(part2), part3, part4)
fun <P1, P2, P3, P4, Q3> Tuple4<P1, P2, P3, P4>.map3(f: (P3) -> Q3): Tuple4<P1, P2, Q3, P4> = Tuple4(part1, part2, f(part3), part4)
fun <P1, P2, P3, P4, Q4> Tuple4<P1, P2, P3, P4>.map4(f: (P4) -> Q4): Tuple4<P1, P2, P3, Q4> = Tuple4(part1, part2, part3, f(part4))
val <P1, P2, P3, P4> Tuple4<P1, P2, P3, P4>._1: P1 get() = part1
val <P1, P2, P3, P4> Tuple4<P1, P2, P3, P4>._2: P2 get() = part2
val <P1, P2, P3, P4> Tuple4<P1, P2, P3, P4>._3: P3 get() = part3
val <P1, P2, P3, P4> Tuple4<P1, P2, P3, P4>._4: P4 get() = part4
fun <P1, P2, P3, P4, P5> Tuple4<P1, P2, P3, P4>.append(part5: P5): Tuple5<P1, P2, P3, P4, P5> = Tuple(part1, part2, part3, part4, part5)
fun <P1, P2, P3, P4, P5, P6> Tuple4<P1, P2, P3, P4>.append(part5: P5, part6: P6): Tuple6<P1, P2, P3, P4, P5, P6> = Tuple(part1, part2, part3, part4, part5, part6)
fun <P1, P2, P3, P4, Q1> Tuple4<P1, P2, P3, P4>.insert1(q1: Q1): Tuple5<Q1, P1, P2, P3, P4> = Tuple5(q1, part1, part2, part3, part4)
fun <P1, P2, P3, P4, Q2> Tuple4<P1, P2, P3, P4>.insert2(q2: Q2): Tuple5<P1, Q2, P2, P3, P4> = Tuple5(part1, q2, part2, part3, part4)
fun <P1, P2, P3, P4, Q3> Tuple4<P1, P2, P3, P4>.insert3(q3: Q3): Tuple5<P1, P2, Q3, P3, P4> = Tuple5(part1, part2, q3, part3, part4)
fun <P1, P2, P3, P4, Q4> Tuple4<P1, P2, P3, P4>.insert4(q4: Q4): Tuple5<P1, P2, P3, Q4, P4> = Tuple5(part1, part2, part3, q4, part4)
fun <P1, P2, P3, P4, Q1> Tuple4<P1, P2, P3, P4>.set1(q1: Q1): Tuple4<Q1, P2, P3, P4> = Tuple4(q1, part2, part3, part4)
fun <P1, P2, P3, P4, Q2> Tuple4<P1, P2, P3, P4>.set2(q2: Q2): Tuple4<P1, Q2, P3, P4> = Tuple4(part1, q2, part3, part4)
fun <P1, P2, P3, P4, Q3> Tuple4<P1, P2, P3, P4>.set3(q3: Q3): Tuple4<P1, P2, Q3, P4> = Tuple4(part1, part2, q3, part4)
fun <P1, P2, P3, P4, Q4> Tuple4<P1, P2, P3, P4>.set4(q4: Q4): Tuple4<P1, P2, P3, Q4> = Tuple4(part1, part2, part3, q4)