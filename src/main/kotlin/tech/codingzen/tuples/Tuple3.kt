package tech.codingzen.tuples

typealias Tuple3<P1, P2, P3> = Triple<P1, P2, P3>
val <P1> Tuple3<P1, *, *>.part1: P1 get() = first
val <P2> Tuple3<*, P2, *>.part2: P2 get() = second
val <P3> Tuple3<*, *, P3>.part3: P3 get() = third
fun <P1, P2, P3, Q1> Tuple3<P1, P2, P3>.map1(f: (P1) -> Q1): Tuple3<Q1, P2, P3> = Tuple3(f(part1), part2, part3)
fun <P1, P2, P3, Q2> Tuple3<P1, P2, P3>.map2(f: (P2) -> Q2): Tuple3<P1, Q2, P3> = Tuple3(part1, f(part2), part3)
fun <P1, P2, P3, Q3> Tuple3<P1, P2, P3>.map3(f: (P3) -> Q3): Tuple3<P1, P2, Q3> = Tuple3(part1, part2, f(part3))
val <P1, P2, P3> Tuple3<P1, P2, P3>._1: P1 get() = part1
val <P1, P2, P3> Tuple3<P1, P2, P3>._2: P2 get() = part2
val <P1, P2, P3> Tuple3<P1, P2, P3>._3: P3 get() = part3
fun <P1, P2, P3, P4> Tuple3<P1, P2, P3>.append(part4: P4): Tuple4<P1, P2, P3, P4> = Tuple(part1, part2, part3, part4)
fun <P1, P2, P3, P4, P5> Tuple3<P1, P2, P3>.append(part4: P4, part5: P5): Tuple5<P1, P2, P3, P4, P5> = Tuple(part1, part2, part3, part4, part5)
fun <P1, P2, P3, P4, P5, P6> Tuple3<P1, P2, P3>.append(part4: P4, part5: P5, part6: P6): Tuple6<P1, P2, P3, P4, P5, P6> = Tuple(part1, part2, part3, part4, part5, part6)
fun <P1, P2, P3, Q1> Tuple3<P1, P2, P3>.insert1(q1: Q1): Tuple4<Q1, P1, P2, P3> = Tuple4(q1, part1, part2, part3)
fun <P1, P2, P3, Q2> Tuple3<P1, P2, P3>.insert2(q2: Q2): Tuple4<P1, Q2, P2, P3> = Tuple4(part1, q2, part2, part3)
fun <P1, P2, P3, Q3> Tuple3<P1, P2, P3>.insert3(q3: Q3): Tuple4<P1, P2, Q3, P3> = Tuple4(part1, part2, q3, part3)
fun <P1, P2, P3, Q1> Tuple3<P1, P2, P3>.set1(q1: Q1): Tuple3<Q1, P2, P3> = Tuple3(q1, part2, part3)
fun <P1, P2, P3, Q2> Tuple3<P1, P2, P3>.set2(q2: Q2): Tuple3<P1, Q2, P3> = Tuple3(part1, q2, part3)
fun <P1, P2, P3, Q3> Tuple3<P1, P2, P3>.set3(q3: Q3): Tuple3<P1, P2, Q3> = Tuple3(part1, part2, q3)