package tech.codingzen.tuples

typealias Tuple2<P1, P2> = Pair<P1, P2>
val <P1> Tuple2<P1, *>.part1: P1 get() = first
val <P2> Tuple2<*, P2>.part2: P2 get() = second
fun <P1, P2, Q1> Tuple2<P1, P2>.map1(f: (P1) -> Q1): Tuple2<Q1, P2> = Tuple2(f(part1), part2)
fun <P1, P2, Q2> Tuple2<P1, P2>.map2(f: (P2) -> Q2): Tuple2<P1, Q2> = Tuple2(part1, f(part2))
val <P1> Tuple2<P1, *>._1: P1 get() = part1
val <P2> Tuple2<*, P2>._2: P2 get() = part2
fun <P1, P2, P3> Tuple2<P1, P2>.append(part3: P3): Tuple3<P1, P2, P3> = Tuple(part1, part2, part3)
fun <P1, P2, P3, P4> Tuple2<P1, P2>.append(part3: P3, part4: P4): Tuple4<P1, P2, P3, P4> = Tuple(part1, part2, part3, part4)
fun <P1, P2, P3, P4, P5> Tuple2<P1, P2>.append(part3: P3, part4: P4, part5: P5): Tuple5<P1, P2, P3, P4, P5> = Tuple(part1, part2, part3, part4, part5)
fun <P1, P2, P3, P4, P5, P6> Tuple2<P1, P2>.append(part3: P3, part4: P4, part5: P5, part6: P6): Tuple6<P1, P2, P3, P4, P5, P6> = Tuple(part1, part2, part3, part4, part5, part6)
fun <P1, P2, Q1> Tuple2<P1, P2>.insert1(q1: Q1): Tuple3<Q1, P1, P2> = Tuple3(q1, part1, part2)
fun <P1, P2, Q2> Tuple2<P1, P2>.insert2(q2: Q2): Tuple3<P1, Q2, P2> = Tuple3(part1, q2, part2)
fun <P1, P2, Q1> Tuple2<P1, P2>.set1(q1: Q1): Tuple2<Q1, P2> = Tuple2(q1, part2)
fun <P1, P2, Q2> Tuple2<P1, P2>.set2(q2: Q2): Tuple2<P1, Q2> = Tuple2(part1, q2)
