package tech.codingzen.tuples

data class Tuple1<out P1>(val part1: P1)
fun <P1, Q1> Tuple1<P1>.map1(f: (P1) -> Q1): Tuple1<Q1> = Tuple1(f(part1))
val <P1> Tuple1<P1>._1: P1 get() = part1
fun <P1, P2> Tuple1<P1>.append(part2: P2): Tuple2<P1, P2> = Tuple(part1, part2)
fun <P1, P2, P3> Tuple1<P1>.append(part2: P2, part3: P3): Tuple3<P1, P2, P3> = Tuple(part1, part2, part3)
fun <P1, P2, P3, P4> Tuple1<P1>.append(part2: P2, part3: P3, part4: P4): Tuple4<P1, P2, P3, P4> = Tuple(part1, part2, part3, part4)
fun <P1, P2, P3, P4, P5> Tuple1<P1>.append(part2: P2, part3: P3, part4: P4, part5: P5): Tuple5<P1, P2, P3, P4, P5> = Tuple(part1, part2, part3, part4, part5)
fun <P1, P2, P3, P4, P5, P6> Tuple1<P1>.append(part2: P2, part3: P3, part4: P4, part5: P5, part6: P6): Tuple6<P1, P2, P3, P4, P5, P6> = Tuple(part1, part2, part3, part4, part5, part6)
fun <P1, Q1> Tuple1<P1>.insert1(q1: Q1): Tuple2<Q1, P1> = Tuple2(q1, part1)
fun <P1, Q1> Tuple1<P1>.set1(q1: Q1): Tuple1<Q1> = Tuple1(q1)
