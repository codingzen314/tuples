package tech.codingzen.tuples

data class Tuple6<out P1, out P2, out P3, out P4, out P5, out P6>(val part1: P1, val part2: P2, val part3: P3, val part4: P4, val part5: P5, val part6: P6)
fun <P1, P2, P3, P4, P5, P6, Q1> Tuple6<P1, P2, P3, P4, P5, P6>.map1(f: (P1) -> Q1): Tuple6<Q1, P2, P3, P4, P5, P6> = Tuple6(f(part1), part2, part3, part4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q2> Tuple6<P1, P2, P3, P4, P5, P6>.map2(f: (P2) -> Q2): Tuple6<P1, Q2, P3, P4, P5, P6> = Tuple6(part1, f(part2), part3, part4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q3> Tuple6<P1, P2, P3, P4, P5, P6>.map3(f: (P3) -> Q3): Tuple6<P1, P2, Q3, P4, P5, P6> = Tuple6(part1, part2, f(part3), part4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q4> Tuple6<P1, P2, P3, P4, P5, P6>.map4(f: (P4) -> Q4): Tuple6<P1, P2, P3, Q4, P5, P6> = Tuple6(part1, part2, part3, f(part4), part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q5> Tuple6<P1, P2, P3, P4, P5, P6>.map5(f: (P5) -> Q5): Tuple6<P1, P2, P3, P4, Q5, P6> = Tuple6(part1, part2, part3, part4, f(part5), part6)
fun <P1, P2, P3, P4, P5, P6, Q6> Tuple6<P1, P2, P3, P4, P5, P6>.map6(f: (P6) -> Q6): Tuple6<P1, P2, P3, P4, P5, Q6> = Tuple6(part1, part2, part3, part4, part5, f(part6))
val <P1, P2, P3, P4, P5, P6> Tuple6<P1, P2, P3, P4, P5, P6>._1: P1 get() = part1
val <P1, P2, P3, P4, P5, P6> Tuple6<P1, P2, P3, P4, P5, P6>._2: P2 get() = part2
val <P1, P2, P3, P4, P5, P6> Tuple6<P1, P2, P3, P4, P5, P6>._3: P3 get() = part3
val <P1, P2, P3, P4, P5, P6> Tuple6<P1, P2, P3, P4, P5, P6>._4: P4 get() = part4
val <P1, P2, P3, P4, P5, P6> Tuple6<P1, P2, P3, P4, P5, P6>._5: P5 get() = part5
val <P1, P2, P3, P4, P5, P6> Tuple6<P1, P2, P3, P4, P5, P6>._6: P6 get() = part6
fun <P1, P2, P3, P4, P5, P6, Q1> Tuple6<P1, P2, P3, P4, P5, P6>.set1(q1: Q1): Tuple6<Q1, P2, P3, P4, P5, P6> = Tuple6(q1, part2, part3, part4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q2> Tuple6<P1, P2, P3, P4, P5, P6>.set2(q2: Q2): Tuple6<P1, Q2, P3, P4, P5, P6> = Tuple6(part1, q2, part3, part4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q3> Tuple6<P1, P2, P3, P4, P5, P6>.set3(q3: Q3): Tuple6<P1, P2, Q3, P4, P5, P6> = Tuple6(part1, part2, q3, part4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q4> Tuple6<P1, P2, P3, P4, P5, P6>.set4(q4: Q4): Tuple6<P1, P2, P3, Q4, P5, P6> = Tuple6(part1, part2, part3, q4, part5, part6)
fun <P1, P2, P3, P4, P5, P6, Q5> Tuple6<P1, P2, P3, P4, P5, P6>.set5(q5: Q5): Tuple6<P1, P2, P3, P4, Q5, P6> = Tuple6(part1, part2, part3, part4, q5, part6)
fun <P1, P2, P3, P4, P5, P6, Q6> Tuple6<P1, P2, P3, P4, P5, P6>.set6(q6: Q6): Tuple6<P1, P2, P3, P4, P5, Q6> = Tuple6(part1, part2, part3, part4, part5, q6)